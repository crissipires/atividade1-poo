package br.ucsal;

import java.util.Scanner;

public class Atividade2 {
    public static void main(String[] args) {
        calculaEscreValorE();
    }

    private static void calculaEscreValorE() {
        double valor = obterValor();
        if (valor > 0) {
            calcularE(valor);
        }
    }

    private static double obterValor() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite um valor (inteiro e positivo):");
        return sc.nextDouble();
    }

    private static void calcularE(double valor) {
        int fatorial = 1;
        double inverso = 1;

        if (valor == 1) {
            imprimirResultado(1);
        } else {
            for (int c = 1; c < valor + 1; c++) {
                fatorial *= c;
                inverso += 1 / (double) fatorial;
            }
            imprimirResultado(inverso);
        }
    }

    public static void imprimirResultado(double valor){
        System.out.printf("E = %.3f",valor);
    }
}