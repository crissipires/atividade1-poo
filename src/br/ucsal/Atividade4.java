package br.ucsal;

import java.util.*;

public class Atividade4 {
    public static void main(String[] args) {
        calculaPessoa();
    }

    private static void calculaPessoa() {
        List<String> nome = new ArrayList<>();
        List<Double> altura = new ArrayList<>();
        List<Double> peso = new ArrayList<>();

        Scanner sc = new Scanner(System.in);

        System.out.println("Digite o nome altura e peso");

        for(int c=0;c<3;c++) {
            nome.add(sc.next());
            altura.add(sc.nextDouble());
            peso.add(sc.nextDouble());
        }

        int index_maxAltura = altura.indexOf(Collections.max(altura));
        int index_maxPeso = peso.indexOf(Collections.max(peso));

        System.out.printf("Pessoa mais pesada - %s com %s kg\n",nome.get(index_maxPeso),peso.get(index_maxPeso));
        System.out.printf("Pessoa mais alta - %s com %s cm ",nome.get(index_maxAltura),altura.get(index_maxAltura));


    }
}
