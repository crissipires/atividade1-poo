package br.ucsal;

import java.util.Arrays;
import java.util.Scanner;

public class Atividade3 {
    public static void main(String[] args) {
        calcularMedia();
    }

    private static void calcularMedia() {

        int[] valores = obterArrayValores();

        int maiorValor = obterMaior(valores);
        int menorValor= obterMenor(valores);
        double media = obterMedia(valores);

        System.out.println("O maior valor é " + maiorValor);
        System.out.println("O menor valor é " + menorValor);
        System.out.print("A Media é " + media);

    }

    private static double obterMedia(int[] valores) {
        int soma = 0;
        for(int valor:valores){
            soma += valor;
        }
        return (double) soma/valores.length ;
    }

    private static int obterMenor(int[] valores) {
        Arrays.sort(valores);
        return valores[0];
    }

    private static int obterMaior(int[] valores) {
        Arrays.sort(valores);
        return valores[9];
    }

    private static int[] obterArrayValores() {
        int[] valores = new int[10];

        Scanner sc = new Scanner(System.in);
        System.out.println("Digite 10 numeros inteiros:");

        for(int c=0;c<10;c++) {
            valores[c] = sc.nextInt();
        }

        return valores;
    }
}
