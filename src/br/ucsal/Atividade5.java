package br.ucsal;

import java.util.Scanner;

public class Atividade5 {
    public static void main(String[] args) {
        calculo();
    }

    private static void calculo() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Digite: n, a1 e r");

        int n = sc.nextInt();
        int a1 = sc.nextInt();
        int r = sc.nextInt();

        int[] arrayTermos = nTermos(a1, n, r);

        imprimirArray(arrayTermos);

        int an = arrayTermos[n-1];

        int soma = somaDosTermos(an,a1,n);

        System.out.print("A soma dos termos é " + soma);
    }

    private static void imprimirArray(int[] arrayTermos) {
        for(int ns:arrayTermos){
            System.out.print(ns + " ");
        }
    }

    private static int somaDosTermos(int an, int a1, int n) {
        return ((a1 + an) * n) / 2;
    }

    private static int[] nTermos(int a1, int n, int r) {
        int[] n_termos = new int[n];

        for (int c = 0; c < n; c++) {
            n_termos[c] = a1 + (c) * r;
        }

        return n_termos;
    }



}

