package br.ucsal;

import java.util.Scanner;

public class Atividade1 {
    public static void main(String[] args) {
        obterNotaCalcularExibirConceito();
    }

    public static void obterNotaCalcularExibirConceito(){
        int nota = obterValor();

        if (nota >= 0 && nota <= 100){
            obterConceito(nota);
        } else {
            System.out.println("[ERRO] Valores fora de parâmetro!");
        }
    }

    public static int obterValor(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite a nota:");
        return sc.nextInt();


    }

    public static void obterConceito(int nota){
        String conceito;
        if(nota <= 50){
            conceito = "Insuficiente";
        } else if (nota <= 64){
            conceito = "Regular";
        } else if (nota <= 84){
            conceito = "Bom";
        } else {
            conceito = "Ótimo";
        }
        exibirConteito(conceito);
    }

    public static void exibirConteito(String coneito){
        System.out.println("O conceito do aluno é " + coneito);
    }
}
